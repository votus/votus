﻿using System;

namespace VOtus.NewsService.DAL.Models
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}