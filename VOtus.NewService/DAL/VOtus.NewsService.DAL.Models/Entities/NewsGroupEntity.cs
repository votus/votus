﻿using System;

namespace VOtus.NewsService.DAL.Models.Entities
{
    public class NewsGroupEntity : BaseEntity
    {
        public Guid NewsId { get; set; }

        public Guid GroupId { get; set; }
    }
}