﻿using System;

namespace VOtus.NewsService.DAL.Models.Entities
{
    public class UserEntity : BaseEntity
    {        
        public bool IsDeleted { get; set; }
    }
}