﻿using System;

namespace VOtus.NewsService.DAL.Models.Entities
{
    public class NewsEntity : BaseEntity
    {        
        public Guid UserId { get; set; }
        
        public string Content { get; set; }
        
        public DateTime AddedDate { get; set; }
        
        public DateTime ModifiedDate { get; set; }

        public bool IsDeleted { get; set; }
     
        public bool CreatedOn { get; set; }
        
        public bool ModifiedOn { get; set; }
    }
}