﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using VOtus.NewsService.DAL.Models;

namespace VOtus.NewsService.DAL.Abstractions.Repositories
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<IEnumerable<T>> GetAllWithPagingAsync(int pageSize, int pageIndex);
        
        Task<T> GetByIdAsync(Guid id);
        
        Task<long> GetCountAsync();
        
        Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids);
        
        Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate);
        
        Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate);

        Task<Guid> AddAsync(T entity);

        Task UpdateAsync(T entity);

        Task DeleteAsync(T entity);
    }
}