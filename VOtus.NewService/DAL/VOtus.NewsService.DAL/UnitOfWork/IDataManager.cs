﻿using System.Threading.Tasks;
using VOtus.NewsService.DAL.Abstractions.Repositories;
using VOtus.NewsService.DAL.Models.Entities;

namespace VOtus.NewsService.DAL.UnitOfWork
{
    public interface IDataManager
    {
        IRepository<NewsEntity> NewsRepository { get; }
        IRepository<NewsGroupEntity> NewsGroupRepository { get; }
        IRepository<UserEntity> UserRepository { get; }
        Task SaveAsync();
    }
}