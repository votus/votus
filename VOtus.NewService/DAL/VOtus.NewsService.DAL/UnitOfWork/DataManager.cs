﻿using System;
using System.Threading.Tasks;
using VOtus.NewsService.DAL.Abstractions.Repositories;
using VOtus.NewsService.DAL.Models.Entities;

namespace VOtus.NewsService.DAL.UnitOfWork
{
    public class DataManager : IDataManager
    {
        public IRepository<NewsEntity> NewsRepository { get; }
        public IRepository<NewsGroupEntity> NewsGroupRepository { get; }
        public IRepository<UserEntity> UserRepository { get; }
        private readonly DataContext _dataContext;

        public DataManager(DataContext dataContext, IRepository<NewsEntity> newsRepository,
            IRepository<NewsGroupEntity> newsGroupRepository, IRepository<UserEntity> userRepository)
        {
            NewsRepository = newsRepository ?? throw new ArgumentNullException(nameof(newsRepository));
            NewsGroupRepository = newsGroupRepository ?? throw new ArgumentNullException(nameof(newsGroupRepository));
            UserRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _dataContext = dataContext;
        }

        public async Task SaveAsync()
        {
            await _dataContext.SaveChangesAsync();
        }
    }
}