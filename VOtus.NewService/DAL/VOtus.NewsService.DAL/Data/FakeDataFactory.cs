﻿using System;
using System.Collections.Generic;
using VOtus.NewsService.DAL.Models.Entities;

namespace VOtus.NewsService.DAL.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<NewsEntity> Newses => new List<NewsEntity>()
        {
            new NewsEntity()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                UserId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Content = "Первая новость",
                AddedDate = new DateTime(2020, 2, 3),
                ModifiedDate = new DateTime(2020, 2, 3),
                IsDeleted = false,
                CreatedOn = true,
                ModifiedOn = true
            },
            new NewsEntity()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                UserId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Content = "Вторая новость",
                AddedDate = new DateTime(2020, 3, 4),
                ModifiedDate = new DateTime(2020, 5, 6),
                IsDeleted = false,
                CreatedOn = true,
                ModifiedOn = true
            }
        };

        public static IEnumerable<NewsGroupEntity> NewsGroups => new List<NewsGroupEntity>()
        {
            new NewsGroupEntity()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                GroupId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                NewsId = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02")
            }
        };
        
        public static IEnumerable<UserEntity> User => new List<UserEntity>()
        {
            new UserEntity()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                IsDeleted = false
            },
            new UserEntity()
            {
                Id = Guid.Parse("24829686-a368-4eeb-8bfa-dd69b6050d02"),
                IsDeleted = true
            }
        };
    }
}