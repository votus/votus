﻿namespace VOtus.NewsService.DAL.Data
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}