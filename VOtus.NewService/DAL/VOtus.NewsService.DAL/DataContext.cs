﻿using Microsoft.EntityFrameworkCore;
using VOtus.NewsService.DAL.Models.Entities;

namespace VOtus.NewsService.DAL
{
    public class DataContext : DbContext
    {
        public DbSet<NewsEntity> Newses { get; set; }

        public DbSet<NewsGroupEntity> NewsGroups { get; set; }
        
        public DbSet<UserEntity> Users { get; set; }
        
        public DataContext()
        {
            
        }
        
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }
    }
}