﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VOtus.NewsService.BLL.Abstractions.Services;
using VOtus.NewsService.BLL.Models;
using VOtus.NewsService.DAL.Abstractions.Repositories;
using VOtus.NewsService.DAL.Models.Entities;

namespace VOtus.NewsService.BLL.Services
{
    public class NewsService : INewsService
    {
        private readonly IRepository<NewsEntity> _newsRepo;
        private readonly IRepository<NewsGroupEntity> _newsGroupRepo;

        public NewsService(IRepository<NewsEntity> news, IRepository<NewsGroupEntity> newsGroup)
        {
            _newsRepo = news ?? throw new ArgumentNullException(nameof(news));
            _newsGroupRepo = newsGroup ?? throw new ArgumentNullException(nameof(newsGroup));
        }

        public async Task<List<NewsResponse>> GetAllAsync(CancellationToken cancellationToken = default)
        {
            var entities = await _newsRepo.GetAllAsync();
            var result = new List<NewsResponse>();
            entities.ToList().ForEach(x => result.Add(new NewsResponse
            {
                Id = x.Id,
                Content = x.Content,
                AddedDate = x.AddedDate,
                CreatedOn = x.CreatedOn,
                IsDeleted = x.IsDeleted,
                ModifiedDate = x.ModifiedDate,
                ModifiedOn = x.ModifiedOn,
                UserId = x.UserId,
                GroupId = GetGroupId(x.Id).Result
            }));
            return result;
        }

        public async Task<PaginatedItemsGeneric<NewsResponse>> GetAllWithPagingAsync(int pageSize, int pageIndex,
            CancellationToken cancellationToken = default)
        {
            var totalCount = await _newsRepo.GetCountAsync();

            var entities = await _newsRepo.GetAllWithPagingAsync(pageSize, pageIndex);
            var data = new List<NewsResponse>();
            entities.ToList().ForEach(x => data.Add(new NewsResponse()
            {
                Id = x.Id,
                Content = x.Content,
                AddedDate = x.AddedDate,
                CreatedOn = x.CreatedOn,
                IsDeleted = x.IsDeleted,
                ModifiedDate = x.ModifiedDate,
                ModifiedOn = x.ModifiedOn,
                UserId = x.UserId,
                GroupId = GetGroupId(x.Id).Result
            }));

            return new PaginatedItemsGeneric<NewsResponse>(pageIndex, pageSize, totalCount, data);
        }

        private async Task<Guid?> GetGroupId(Guid newsId)
        {
            var group =  await _newsGroupRepo.GetFirstWhere(w => w.NewsId == newsId);
            return group?.GroupId;
        }

        public async Task<NewsResponse> GetByIdAsync(Guid id, CancellationToken cancellationToken = default)
        {
            var entity = await _newsRepo.GetByIdAsync(id);
            var result = new NewsResponse
            {
                Id = entity.Id,
                Content = entity.Content,
                AddedDate = entity.AddedDate,
                CreatedOn = entity.CreatedOn,
                IsDeleted = entity.IsDeleted,
                ModifiedDate = entity.ModifiedDate,
                ModifiedOn = entity.ModifiedOn,
                UserId = entity.UserId,
                GroupId = GetGroupId(entity.Id).Result
            };
            return result;
        }

        public async Task<List<NewsResponse>> GetByFilterAsync(FilterNews filter, CancellationToken cancellationToken = default)
        {
            IEnumerable<NewsEntity> entities;

            if (filter.GroupId.HasValue)
            {
                var newsByGroup = (List<NewsGroupEntity>) await _newsGroupRepo
                    .GetWhere(x => x.GroupId == filter.GroupId);
                entities = await _newsRepo
                    .GetWhere(x => x.Id == (filter.NewsId ?? x.Id) &&
                                   x.UserId == (filter.UserId ?? x.UserId) &&
                                   newsByGroup.Select(s => s.NewsId).Contains(x.Id));
            }
            else
            {
                entities = await _newsRepo
                    .GetWhere(x => x.Id == (filter.NewsId ?? x.Id) &&
                                   x.UserId == (filter.UserId ?? x.UserId));
            }

            var result = new List<NewsResponse>();
            entities.ToList().ForEach(x => result.Add(new NewsResponse()
            {
                Id = x.Id,
                Content = x.Content,
                AddedDate = x.AddedDate,
                CreatedOn = x.CreatedOn,
                IsDeleted = x.IsDeleted,
                ModifiedDate = x.ModifiedDate,
                ModifiedOn = x.ModifiedOn,
                UserId = x.UserId,
                GroupId = GetGroupId(x.Id).Result
            }));
            return result;
        }

        public async Task DeleteAsync(Guid id, CancellationToken cancellationToken = default)
        {
            var news = await _newsRepo.GetByIdAsync(id);
            news.IsDeleted = true;
            await _newsRepo.UpdateAsync(news);
        }

        public async Task UpdateAsync(News news, CancellationToken cancellationToken = default)
        {
            var newsEntity = await _newsRepo.GetByIdAsync(news.Id);
            newsEntity.Content = news.Content;
            newsEntity.ModifiedDate = DateTime.UtcNow;

            await _newsRepo.UpdateAsync(newsEntity);
        }

        public async Task<Guid> CreateAsync(News news, CancellationToken cancellationToken = default)
        {
            var createDay = DateTime.UtcNow;
            var newsEntity = new NewsEntity
            {
                Content = news.Content,
                AddedDate = createDay,
                CreatedOn = true,
                IsDeleted = false,
                ModifiedDate = createDay,
                ModifiedOn = true,
                UserId = news.UserId
            };

            var newsId = await _newsRepo.AddAsync(newsEntity);

            if (!news.GroupId.HasValue) return newsId;
            
            var newsGroupEntity = new NewsGroupEntity
            {
                GroupId = (Guid) news.GroupId,
                NewsId = newsId
            };
            await _newsGroupRepo.AddAsync(newsGroupEntity);

            return newsId;
        }
    }
}