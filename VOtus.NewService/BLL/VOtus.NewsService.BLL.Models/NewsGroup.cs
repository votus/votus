﻿using System;

namespace VOtus.NewsService.BLL.Models
{
    public class NewsGroup
    {
        public Guid Id { get; set; }
        
        public Guid NewsId { get; set; }

        public Guid GroupId { get; set; }
    }
}