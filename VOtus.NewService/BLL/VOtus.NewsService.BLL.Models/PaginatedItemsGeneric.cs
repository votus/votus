﻿using System.Collections.Generic;

namespace VOtus.NewsService.BLL.Models
{
    public class PaginatedItemsGeneric<TEntity> where TEntity : class
    {
        public int PageIndex { get; private set; }

        public int PageSize { get; private set; }

        public long TotalCount { get; private set; }

        public IEnumerable<TEntity> Data { get; private set; }

        public PaginatedItemsGeneric(int pageIndex, int pageSize, long totalCount, IEnumerable<TEntity> data)
        {
            this.PageIndex = pageIndex;
            this.PageSize = pageSize;
            this.TotalCount = totalCount;
            this.Data = data;
        }
    }
}