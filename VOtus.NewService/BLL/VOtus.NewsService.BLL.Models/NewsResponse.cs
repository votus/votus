﻿using System;
using VOtus.NewsService.DAL.Models;

namespace VOtus.NewsService.BLL.Models
{
    public class NewsResponse : BaseEntity
    {
        public Guid UserId { get; set; }
        
        public Guid? GroupId { get; set; }
        
        public string Content { get; set; }
        
        public DateTime AddedDate { get; set; }
        
        public DateTime ModifiedDate { get; set; }

        public bool IsDeleted { get; set; }
     
        public bool CreatedOn { get; set; }
        
        public bool ModifiedOn { get; set; }
    }
}