﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VOtus.NewsService.BLL.Models
{
    public class FilterNews
    {        
        public Guid? NewsId { get; set; }
        
        public Guid? UserId { get; set; }
        
        public Guid? GroupId { get; set; }

        public bool IsEmpty()
        {
            return !(NewsId.HasValue || UserId.HasValue || GroupId.HasValue);
        }
    }
}