﻿using System;

namespace VOtus.NewsService.BLL.Models
{
    public class News
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public Guid? GroupId { get; set; }

        public string Content { get; set; }

        public DateTime AddedDate { get; set; }

        public DateTime ModifiedDate { get; set; }

        public bool IsDeleted { get; set; }

        public bool CreatedOn { get; set; }

        public bool ModifiedOn { get; set; }

        public News() { }

        public News(Guid userId, Guid? groupId, string content)
        {
            UserId = userId;
            GroupId = groupId;
            Content = content;
        }
    }
}