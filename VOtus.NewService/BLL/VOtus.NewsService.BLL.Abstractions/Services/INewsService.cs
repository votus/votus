﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using VOtus.NewsService.BLL.Models;

namespace VOtus.NewsService.BLL.Abstractions.Services
{
    public interface INewsService
    {
        Task<List<NewsResponse>> GetAllAsync(CancellationToken cancellationToken = default);
        
        Task<PaginatedItemsGeneric<NewsResponse>> GetAllWithPagingAsync(int pageSize, int pageIndex, CancellationToken cancellationToken = default);

        Task<NewsResponse> GetByIdAsync(Guid id, CancellationToken cancellationToken = default);
        
        Task<List<NewsResponse>> GetByFilterAsync(FilterNews filter, CancellationToken cancellationToken = default);

        Task<Guid> CreateAsync(News news, CancellationToken cancellationToken = default);
        
        Task UpdateAsync(News news, CancellationToken cancellationToken = default);
        
        Task DeleteAsync(Guid id, CancellationToken cancellationToken = default);
    }
}