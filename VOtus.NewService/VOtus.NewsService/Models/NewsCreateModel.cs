﻿using System;
using System.ComponentModel.DataAnnotations;
using VOtus.NewsService.DAL.Models;

namespace VOtus.NewsService.Models
{
    public class NewsCreateModel
    {
        public Guid UserId { get; set; }
        
        public Guid? GroupId { get; set; }
        
        [Required]
        public string Content { get; set; }
    }
}