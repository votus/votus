﻿using System;
using System.ComponentModel.DataAnnotations;
using VOtus.NewsService.DAL.Models;

namespace VOtus.NewsService.Models
{
    public class NewsEditModel
    {
        public Guid Id { get; set; }

        [Required]
        public string Content { get; set; }
    }
}