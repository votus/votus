﻿using System;
using System.ComponentModel.DataAnnotations;
using VOtus.NewsService.DAL.Models;

namespace VOtus.NewsService.Models
{
    public static class ApiRoutes
    {
        public static class News
        {
            public const string Tidings = "News";
            public const string TidingsFilter = "NewsFilter";
        }
    }
}