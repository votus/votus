﻿using MassTransit;
using MassTransit.Definition;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using VOtus.NewsService.MassTransit;

namespace VOtus.NewsService.ConfigureService
{
    public class ConfigureServiceMassTransit
    {
        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            services.AddMassTransit(x =>
            {
                x.AddBus(busFactory =>
                {
                    var bus = Bus.Factory.CreateUsingRabbitMq(cfg =>
                    {
                        cfg.Host(Settings.MassTransit.Url, 5672, Settings.MassTransit.Host, configurator =>
                        {
                            configurator.Username(Settings.MassTransit.UserName);
                            configurator.Password(Settings.MassTransit.Password);
                        });

                        cfg.ConfigureEndpoints(busFactory, KebabCaseEndpointNameFormatter.Instance);

                        cfg.UseJsonSerializer();

                        cfg.UseHealthCheck(busFactory);
                    });

                    return bus;
                });

                x.AddConsumer<ApplicationUserCreatedConsumer>(typeof(ApplicationUserConsumerDefinition.ApplicationUserCreatedConsumerDefinition));
                x.AddConsumer<ApplicationUserEditedConsumer>(typeof(ApplicationUserConsumerDefinition.ApplicationUserEditedConsumerDefinition));
                x.AddConsumer<ApplicationUserDeletedConsumer>(typeof(ApplicationUserConsumerDefinition.ApplicationUserDeletedConsumerDefinition));
            });
            services.AddMassTransitHostedService();
        }
    }
}