﻿using AutoMapper;
using BrokerContracts.User;
using VOtus.NewsService.BLL.Models;
using VOtus.NewsService.DAL.Models.Entities;

namespace VOtus.NewsService.Infrastructure
{
    public class MappingApiProfile : Profile
    {
        public MappingApiProfile()
        {
            CreateMap<NewsResponse, News>();
            
            CreateMap<UserEntity, UserCreated>()
                .ReverseMap();
            
            CreateMap<UserEntity, UserEdited>()
                .ReverseMap();
        }
    }

}
