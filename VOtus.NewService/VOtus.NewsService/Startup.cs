using System;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using VOtus.NewsService.BLL.Abstractions.Services;
using VOtus.NewsService.ConfigureService;
using VOtus.NewsService.DAL;
using VOtus.NewsService.DAL.Abstractions.Repositories;
using VOtus.NewsService.DAL.Data;
using VOtus.NewsService.DAL.Repositories;
using VOtus.NewsService.DAL.UnitOfWork;
using VOtus.NewsService.Infrastructure;

namespace VOtus.NewsService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public Microsoft.Extensions.Configuration.IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {            
            services.AddControllers().AddMvcOptions(x=> 
                x.SuppressAsyncSuffixInActionNames = false);
            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddScoped<IDbInitializer, EfDbInitializer>();
            services.AddScoped<INewsService, BLL.Services.NewsService>();
            
            services.AddDbContext<DataContext>(x =>
            {
                 x.UseNpgsql(Configuration.GetConnectionString("NewsServiceDB"));
                x.UseLazyLoadingProxies();
            });
            
            ConfigureServiceMassTransit.Configure(services, Configuration);
            ConfigureServiceSwagger.Configure(services, Configuration);

            services.AddTransient(typeof(IDataManager), typeof(DataManager));
            
            // Auto Mapper Configurations
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingApiProfile());
            });

            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(op =>
            {
                op.AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowAnyOrigin();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "NewsService v1"));

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            
            //dbInitializer.InitializeDb();
        }
    }
}
