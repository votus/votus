﻿using BrokerContracts;
using MassTransit.Definition;

namespace VOtus.NewsService.MassTransit
{
    public class ApplicationUserConsumerDefinition
    {
        public class ApplicationUserCreatedConsumerDefinition : ConsumerDefinition<ApplicationUserCreatedConsumer>
        {
            public ApplicationUserCreatedConsumerDefinition()
            {
                EndpointName = Constants.NotificationQueueCore;
            }
        }
        
        public class ApplicationUserDeletedConsumerDefinition : ConsumerDefinition<ApplicationUserDeletedConsumer>
        {
            public ApplicationUserDeletedConsumerDefinition()
            {
                EndpointName = Constants.NotificationQueueCore;
            }
        }
        
        public class ApplicationUserEditedConsumerDefinition : ConsumerDefinition<ApplicationUserEditedConsumer>
        {
            public ApplicationUserEditedConsumerDefinition()
            {
                EndpointName = Constants.NotificationQueueCore;
            }
        }
    }
}