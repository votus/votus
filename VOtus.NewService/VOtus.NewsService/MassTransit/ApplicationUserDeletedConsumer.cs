﻿using System.Threading.Tasks;
using AutoMapper;
using BrokerContracts.User;
using MassTransit;
using Microsoft.Extensions.Logging;
using VOtus.NewsService.DAL.Models.Entities;
using VOtus.NewsService.DAL.UnitOfWork;

namespace VOtus.NewsService.MassTransit
{
    public class ApplicationUserDeletedConsumer : IConsumer<UserDeleted>
    {
        private readonly IDataManager _dataManager;
        private readonly ILogger<ApplicationUserDeletedConsumer> _logger;
        private readonly IMapper _mapper;

        public ApplicationUserDeletedConsumer(IDataManager dataManager, IMapper mapper,
            ILogger<ApplicationUserDeletedConsumer> logger)
        {
            _dataManager = dataManager;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<UserDeleted> context)
        {
            var model = _mapper.Map<UserEntity>(context.Message);
            _logger.LogInformation("Get message from broker. Deleted user: {@User}", model);
            await _dataManager.UserRepository.UpdateAsync(model);
        }
    }
}