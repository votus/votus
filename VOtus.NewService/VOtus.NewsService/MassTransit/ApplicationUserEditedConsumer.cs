﻿using System.Threading.Tasks;
using AutoMapper;
using BrokerContracts.User;
using MassTransit;
using Microsoft.Extensions.Logging;
using VOtus.NewsService.DAL.Models.Entities;
using VOtus.NewsService.DAL.UnitOfWork;

namespace VOtus.NewsService.MassTransit
{
    public class ApplicationUserEditedConsumer  : IConsumer<UserEdited>
    {
        private readonly IDataManager _dataManager;
        private readonly ILogger<ApplicationUserEditedConsumer> _logger;
        private readonly IMapper _mapper;

        public ApplicationUserEditedConsumer(IDataManager dataManager, IMapper mapper,
            ILogger<ApplicationUserEditedConsumer> logger)
        {
            _dataManager = dataManager;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<UserEdited> context)
        {
            var model = _mapper.Map<UserEntity>(context.Message);
            _logger.LogInformation("Get message from broker. Edit user: {@User}", model);
            var user = _dataManager.UserRepository.GetByIdAsync(model.Id);
            if (user is null)
            {
                await _dataManager.UserRepository.AddAsync(model);
            }
        }
    }
}