﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using VOtus.NewsService.BLL.Abstractions.Services;
using VOtus.NewsService.BLL.Models;
using VOtus.NewsService.Models;

namespace VOtus.NewsService.Controllers
{
    [Produces("application/json")]
    [ApiController]
    public class NewsController : ControllerBase
    {
        private readonly INewsService _newsService;
        private readonly IMapper _mapper;

        public NewsController(INewsService newsService, IMapper mapper)
        {
            _newsService = newsService ?? throw new ArgumentNullException(nameof(newsService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        ///     Получить список новостей. С пагинацией.
        /// </summary>
        /// <remarks>
        ///     Sample request:
        ///     GET /api/v1/News?pageSize=10&amp;pageIndex=0
        /// </remarks>
        /// <param name="pageSize"> Количество записей на странице. </param>
        /// <param name="pageIndex"> Номер страницы. </param>
        /// <returns>Контейнер, содержащий список c новостями</returns>
        /// <response code="200">Запрос выполнился успешно, возвращены данные</response>
        /// <response code="204">Запрос выполнился успешно, но данные не найдены</response>
        /// <response code="500">Внутренняя ошибка сервера</response>        
        [HttpGet(ApiRoutes.News.Tidings)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<PaginatedItemsGeneric<NewsResponse>>> GetAsync([FromQuery] int pageSize = 10, [FromQuery] int pageIndex = 0)
        {
            var response = await _newsService.GetAllWithPagingAsync(pageSize, pageIndex);
            return Ok(response);
        }

        /// <summary>
        ///     Получить список новостей по фильтру
        /// </summary>
        /// <param name="filter"> Модель фильтра, содержащая параметры фильтрации. </param>
        /// <returns>Контейнер, содержащий список c новостями</returns>
        /// <response code="200">Запрос выполнился успешно, возвращены данные</response>
        /// <response code="204">Запрос выполнился успешно, но данные не найдены</response>
        /// <response code="500">Внутренняя ошибка сервера</response>
        [HttpGet(ApiRoutes.News.TidingsFilter)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<NewsResponse>> GetByIdAsync([FromQuery] FilterNews filter)
        {
            if (filter.IsEmpty())
            {
                return BadRequest("Arguments are empty");
            }

            var news = await _newsService.GetByFilterAsync(filter);

            return Ok(news);
        }

        /// <summary>
        ///     Создать новость
        /// </summary>
        /// <remarks>
        /// 
        ///     Sample request:
        ///     POST /api/v1/News/
        ///     {
        ///         "userId": "f766e2bf-340a-46ea-bff3-f1700b435895",
        ///         "groupId": "a0e8735d-0750-4a83-9f14-831038279e3f",
        ///         "content": "Текст статьи."
        ///     }
        /// 
        /// </remarks>
        /// <param name="model"> Модель, содержащая новость. </param>
        /// <response code="200">Запрос выполнился успешно, создана запись</response>
        /// <response code="400">Некорректные данные</response>
        /// <response code="500">Внутренняя ошибка сервера</response>
        [HttpPost(ApiRoutes.News.Tidings)]
        public async Task<ActionResult> CreateNewsAsync(NewsCreateModel model)
        {
            var news = new News(model.UserId, model.GroupId, model.Content);
            var id = await _newsService.CreateAsync(news);

            var response = await _newsService.GetByIdAsync(id);

            return Created("", response);
        }

        /// <summary>
        ///     Редактировать новость
        /// </summary>
        /// <remarks>
        /// 
        ///     Sample request:
        ///     POST /api/v1/News/
        ///     {
        ///         "id": "53729686-a368-4eeb-8bfa-cc69b6050d02",
        ///         "content": "Вторая новость обновленная"
        ///     }
        /// 
        /// </remarks>
        /// <param name="model"> Модель, содержащая редактируемую новость. </param>
        /// <response code="200">Запрос выполнился успешно, запись отредактирована</response>
        /// <response code="400">Некорректные данные</response>
        /// <response code="500">Внутренняя ошибка сервера</response>
        [HttpPut(ApiRoutes.News.Tidings)]
        public async Task<ActionResult<NewsResponse>> EditNewsAsync(NewsEditModel model)
        {
            var news = await _newsService.GetByIdAsync(model.Id);

            if (news == null)
                return NotFound(new { Message = $"News with id {model.Id} not found." });

            news.Content = model.Content;
            var updateModel = _mapper.Map<News>(news);
            await _newsService.UpdateAsync(updateModel);

            return Ok();
        }

        /// <summary>
        ///     Удалить новость
        /// </summary>
        /// <param name="id"> Id новости. </param>
        /// <response code="200">Запрос выполнился успешно, запись удалена</response>
        /// <response code="400">Некорректные данные</response>
        /// <response code="500">Внутренняя ошибка сервера</response>
        [HttpDelete(ApiRoutes.News.Tidings + "{id:guid}")]
        public async Task<IActionResult> DeleteNewsAsync(Guid id)
        {
            var news = await _newsService.GetByIdAsync(id);

            if (news == null)
                return NotFound(new { Message = $"News with id {id} not found." });

            await _newsService.DeleteAsync(id);

            return NoContent();
        }
    }
}