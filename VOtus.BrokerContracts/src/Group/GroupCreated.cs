﻿using System;

namespace BrokerContracts.Group
{
    public class GroupCreated
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
