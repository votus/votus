﻿using System;

namespace BrokerContracts.Group
{
    public class GroupDeleted
    {
        public Guid Id { get; set; }
    }
}
