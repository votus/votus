﻿using System;

namespace BrokerContracts.User
{
    public class UserEdited
    {
        public Guid Id { get; set; }
        
        public string FirstName { get; set; }

        public string SecondName { get; set; }

        public string DateOfBirth { get; set; }

        public string Email { get; set; }
        
        public int Gender { get; set; }
        
        public string Country { get; set; }
    }
}