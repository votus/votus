﻿using System;

namespace BrokerContracts.User
{
    public class UserDeleted
    {
        public Guid Id { get; set; }
    }
}