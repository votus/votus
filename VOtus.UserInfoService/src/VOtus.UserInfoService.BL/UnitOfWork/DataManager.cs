﻿using System.Threading.Tasks;
using VOtus.UserInfoService.BL.Repositorys;
using VOtus.UserInfoService.DAL;

namespace VOtus.UserInfoService.BL.UnitOfWork
{
    public class DataManager : IDataManager
    {
        private readonly DataContext _dataContext;
        public IUserRepository UserRepository { get; }

        public DataManager(DataContext dataContext)
        {
            _dataContext = dataContext;
            UserRepository = new UserRepository(_dataContext);
        }
        
        public async Task SaveAsync()
        {
            await _dataContext.SaveChangesAsync();
        }
    }
}