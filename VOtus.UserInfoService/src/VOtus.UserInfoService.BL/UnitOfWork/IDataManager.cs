﻿using System.Threading.Tasks;
using VOtus.UserInfoService.BL.Repositorys;

namespace VOtus.UserInfoService.BL.UnitOfWork
{
    public interface IDataManager
    {
        IUserRepository UserRepository { get; }
        
        Task SaveAsync();
    }
}