﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VOtus.UserInfoService.DAL;
using VOtus.UserInfoService.DAL.Domain;

namespace VOtus.UserInfoService.BL.Repositorys
{
    public class UserRepository : IUserRepository
    {
        private readonly DataContext _dataContext;

        private IQueryable<User> Users => _dataContext.Users.AsQueryable();

        public UserRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        
        /// <summary>
        /// Create user.
        /// </summary>
        /// <param name="user"> Return created model</param>
        /// <returns></returns>
        public async Task<User> CreateUserAsync(User user)
        {
            user.CreatedAt = DateTime.UtcNow;
            var result = await _dataContext.Users.AddAsync(user);
            return result.Entity;
        }

        /// <summary>
        /// Update user model
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> UpdateUserAsync(User entity)
        {
            try
            {
                entity.ModifiedAt = DateTime.UtcNow;
                _dataContext.Update(entity);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        /// <summary>
        /// Get all user with simple pagination.
        /// Result ordered by Second name
        /// </summary>
        /// <param name="pageSize">Number of records per page</param>
        /// <param name="pageNumber">Page number</param>
        /// <returns></returns>
        public async Task<List<User>> GetAllUserAsync(int pageSize, int pageNumber)
        {
            //Constraints
            if (pageSize < 0) pageSize = 20;
            if (pageSize > 50) pageSize = 50;
            if (pageNumber < 1) pageNumber = 1;
            
            var users = await _dataContext.Users.OrderBy(x=>x.SecondName)
                                                        .Skip(pageSize * (pageNumber - 1))
                                                        .Take(pageSize)
                                                        .ToListAsync();
            return users;
        }

        /// <summary>
        /// Returns the user by id
        /// </summary>
        /// <param name="userId">Id to search</param>
        /// <returns></returns>
        public async Task<User> GetUserByIdAsync(Guid userId)
        {
            var user = await Users.FirstOrDefaultAsync(u => u.Id == userId);
            return user;
        }

        /// <summary>
        /// Returns the user by email
        /// </summary>
        /// <param name="email">Email to search</param>
        /// <returns></returns>
        public async Task<User> GetUserByEmailAsync(string email)
        {
            return await Users.FirstOrDefaultAsync(u => u.Email == email);
        }

        /// <summary>
        /// The method marks the user as deleted.
        /// But does not delete from the base.
        /// </summary>
        /// <param name="userId">user id will be marked as deleted</param>
        /// <returns></returns>
        public async Task<bool> RemoveUserAsync(Guid userId)
        {
            try
            {
                var user = await GetUserByIdAsync(userId);
                user.IsDeleted = true;
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// 1. Method adds user1 as a friend to user2
        /// 2. Checks if user2 is a friend of user1
        /// 3. If yes, then approves friendship
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="friendId"></param>
        /// <returns></returns>
        public async Task<bool> AddToFriendsAsync(Guid userId, Guid friendId)
        {
            try
            {
                //Get user1 by userId
                var user1 = await GetUserByIdAsync(userId);
                //Get user2 by friendId
                var user2 = await GetUserByIdAsync(friendId);
 
                //Creates a friendship record
                user1.Friends.Add(new Friends
                {
                    User = user1,
                    Friend = user2,
                    CreatedAt = DateTime.UtcNow,
                    IsApproval = false
                });


                //Checks if user2 is a friend of user1
                var isFriend = await IsFriend(friendId, userId);
                if (!isFriend) return true;
                
                //Get a friendship records
                var friend1 = user1.Friends.FirstOrDefault(x => x.Friend.Id == user2.Id);
                var friend2 = user2.Friends.FirstOrDefault(x => x.Friend.Id == user1.Id);
                
                if (friend1 == null || friend2 == null) return true;
                
                //Approves friendship
                friend1.IsApproval = true;
                friend2.IsApproval = true;
                var approvalDate = DateTime.UtcNow;
                friend1.ApprovalDate = approvalDate;
                friend2.ApprovalDate = approvalDate;

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            
        }

        /// <summary>
        /// Checks if user1 is a friend of user2
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="friendId"></param>
        /// <returns></returns>
        public async Task<bool> IsFriend(Guid userId, Guid friendId)
        {
            var user = await GetUserByIdAsync(userId);
            var result = user.Friends.FirstOrDefault(x => x.Friend.Id == friendId);
            return result != null;
        }

        /// <summary>
        /// Method removes the friendship record
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="friendId"></param>
        /// <returns></returns>
        public async Task<bool> RemoveFromFriendsAsync(Guid userId, Guid friendId)
        {
            try
            {
                var user = await GetUserByIdAsync(userId);
                var friend = user.Friends.FirstOrDefault(f => f.Friend.Id == friendId);
                user.Friends.Remove(friend);
                
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        /// <summary>
        /// Method adds the user to the blacklist
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="friendId"></param>
        /// <returns></returns>
        public async Task<bool> AddToBlackListAsync(Guid userId, Guid friendId)
        {
            try
            {
                var user = await GetUserByIdAsync(userId);
                var friend = await GetUserByIdAsync(friendId);
 
                user.BlackList.Add(new BlackList()
                {
                    User = user,
                    Friend = friend,
                    CreatedAt = DateTime.UtcNow
                    
                });
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        /// <summary>
        /// Method removes the user from the blacklist
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="friendId"></param>
        /// <returns></returns>
        public async Task<bool> RemoveFromBlackListAsync(Guid userId, Guid friendId)
        {
            try
            {
                var user = await GetUserByIdAsync(userId);

                var friend = user.BlackList.FirstOrDefault(f => f.Friend.Id == friendId);

                user.BlackList.Remove(friend);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
        
        
        /// <summary>
        /// Get all friends with simple pagination.
        /// Result ordered by Second name
        /// </summary>
        /// <param name="pageSize">Number of records per page</param>
        /// <param name="pageNumber">Page number</param>
        /// <returns></returns>
        public async Task<List<User>> GetAllFriends(Guid userId, int pageSize, int pageNumber)
        {
            //Constraints
            if (pageSize < 0) pageSize = 20;
            if (pageSize > 50) pageSize = 50;
            if (pageNumber < 1) pageNumber = 1;
            
            var user = await _dataContext.Users.FirstOrDefaultAsync(x=>x.Id == userId);

            var friends = user.Friends.Select(x=> x.Friend)
                                             .OrderBy(x=>x.SecondName)
                                             .Skip(pageSize * (pageNumber - 1))
                                             .Take(pageSize)
                                             .ToList();
            return friends;
        }
        
        public Task<IEnumerable<User>> GetUsersBlackListed(Guid userId)
        {
            throw new NotImplementedException();
        }
    }
}