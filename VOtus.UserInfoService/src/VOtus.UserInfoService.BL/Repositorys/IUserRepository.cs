﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VOtus.UserInfoService.DAL.Domain;

namespace VOtus.UserInfoService.BL.Repositorys
{
    public interface IUserRepository
    {
        Task<User> CreateUserAsync(User user);
        Task<bool> UpdateUserAsync(User entity);
        Task<List<User>> GetAllUserAsync(int pageSize, int pageNumber);
        Task<User> GetUserByIdAsync(Guid userId);
        Task<User> GetUserByEmailAsync(string email);
        Task<bool> RemoveUserAsync(Guid userId);
        Task<bool> AddToFriendsAsync(Guid userId, Guid friendId);
        Task<List<User>> GetAllFriends(Guid userId, int pageSize, int pageNumber);
        Task<bool> IsFriend(Guid userId, Guid friendId);
        Task<bool> RemoveFromFriendsAsync(Guid userId, Guid friendId);
        Task<bool> AddToBlackListAsync(Guid userId, Guid friendId);
        Task<IEnumerable<User>> GetUsersBlackListed(Guid userId);
        Task<bool> RemoveFromBlackListAsync(Guid userId, Guid friendId);
    }
}