﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using VOtus.UserInfoService.BL.UnitOfWork;
using VOtus.UserInfoService.Web.Contracts.Response;
using VOtus.UserInfoService.Web.Contracts.Routes;

namespace VOtus.UserInfoService.Web.Controllers
{
    [Produces("application/json")]
    public class FriendController : ControllerBase
    {
        private readonly IDataManager _dataManager;

        private readonly IMapper _mapper;
        //private readonly Guid _currentUserId = Guid.Parse("8ce9366c-d791-4558-a45a-2a96448d6c4c");

        public FriendController(IDataManager dataManager, IMapper mapper)
        {
            _dataManager = dataManager;
            _mapper = mapper;
        }
        
        
        /// <summary>
        /// Add to friend
        /// </summary>
        /// <param name="friendId">Friend Id</param>
        /// <response code = "200"> Successful request </response>
        /// <response code = "204"> successful request, Email not found </response>
        /// <response code = "400"> Invalid arguments</response>
        [HttpPost]
        [Route(ApiRoutes.Friend.AddToFriends)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> AddToFriend([FromQuery]Guid _currentUserId, [FromQuery]Guid friendId)
        {
            //Check Id
            if(_currentUserId == Guid.Empty || friendId == Guid.Empty)
                return BadRequest(new ErrorResponse
                {
                    Errors = new List<ErrorModel>{new ErrorModel
                    {
                        FieldName = "",
                        Message = $"id cannot be empty"
                    }}
                });
            
            //Check friends id
            var friend = await _dataManager.UserRepository.GetUserByIdAsync(friendId);
            if(friend == null)
                return BadRequest(new ErrorResponse
                {
                    Errors = new List<ErrorModel>{new ErrorModel
                    {
                        FieldName = "",
                        Message = $"User with id={friendId} not found"
                    }}
                });
            
            
            //Check friend already exist in friends
            var isAlreadyFriend = await _dataManager.UserRepository.IsFriend(_currentUserId, friendId);
            if(isAlreadyFriend)
                return BadRequest(new ErrorResponse
                {
                    Errors = new List<ErrorModel>{new ErrorModel
                    {
                        FieldName = "",
                        Message = $"User with id={friendId} already friend"
                    }}
                });
            
            
            //Check add yourself as a friend
            if(_currentUserId == friendId)
                return BadRequest(new ErrorResponse
                {
                    Errors = new List<ErrorModel>{new ErrorModel
                    {
                        FieldName = "",
                        Message = "you can't add yourself as a friend"
                    }}
                });
            
            
            if(_currentUserId == friendId)
                return BadRequest(new ErrorResponse
                {
                    Errors = new List<ErrorModel>{new ErrorModel
                    {
                        FieldName = "",
                        Message = "you can't add yourself as a friend"
                    }}
                });


            var result = await _dataManager.UserRepository.AddToFriendsAsync(_currentUserId, friendId);

            await _dataManager.SaveAsync();
                

            return Ok();
        }
        
        
        /// <summary>
        /// Get to friend
        /// </summary>
        /// <param name="pageSize">Page size</param>
        /// <param name="pageNumber">Page number</param>
        /// <response code = "200"> Successful request </response>
        /// <response code = "204"> successful request, Email not found </response>
        /// <response code = "400"> Invalid arguments</response>
        [HttpGet]
        [Route(ApiRoutes.Friend.GetFriends)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetFriends([FromQuery]Guid _currentUserId, [FromQuery]int pageSize=20, [FromQuery]int pageNumber=1)
        {
            var result = await _dataManager.UserRepository.GetAllFriends(_currentUserId, pageSize, pageNumber);
            
            if (result.Count == 0)
                return NoContent();
            
            var mapped = _mapper.Map<List<UserResponse>>(result);
            return Ok(mapped);
        }
        
        /// <summary>
        /// Remove from friend
        /// </summary>
        /// <param name="friendId">Friend Id</param>
        /// <response code = "200"> Successful request </response>
        /// <response code = "400"> Invalid arguments</response>
        [HttpDelete]
        [Route(ApiRoutes.Friend.RemoveFromFriends)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> RemoveFriend([FromQuery]Guid _currentUserId, [FromQuery]Guid friendId)
        {
            await _dataManager.UserRepository.RemoveFromFriendsAsync(_currentUserId, friendId);
            await _dataManager.UserRepository.RemoveFromFriendsAsync(friendId, _currentUserId);
            
            await _dataManager.SaveAsync();
            
            return Ok();
        }
    }
}