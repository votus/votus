﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BrokerContracts.User;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using VOtus.UserInfoService.BL.UnitOfWork;
using VOtus.UserInfoService.DAL.Domain;
using VOtus.UserInfoService.Web.Contracts.Requests;
using VOtus.UserInfoService.Web.Contracts.Response;
using VOtus.UserInfoService.Web.Contracts.Routes;

namespace VOtus.UserInfoService.Web.Controllers
{
    [Produces("application/json")]
    public class UserController : ControllerBase
    {
        private readonly IDataManager _dataManager;
        private readonly IMapper _mapper;
        private readonly IPublishEndpoint _publishEndpoint;

        public UserController(IDataManager dataManager, IMapper mapper, IPublishEndpoint publishEndpoint)
        {
            _dataManager = dataManager;
            _mapper = mapper;
            _publishEndpoint = publishEndpoint;
        }
        
        
        
        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="request"></param>
        /// <response code = "201"> Successful register user</response>
        /// <response code = "400"> Invalid arguments</response>
        [HttpPut]
        [Route(ApiRoutes.User.Update)]
        [ProducesResponseType(typeof(UserResponse), 201)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Update([FromBody]UpdateUserRequest request)
        {
            var user = await _dataManager.UserRepository.GetUserByIdAsync(request.Id);
            if (user == null) return BadRequest(new ErrorResponse
            {
                Errors = new List<ErrorModel>
                {
                    new ErrorModel
                    {
                        FieldName = "Id",
                        Message = "User does not exists"
                    }
                }
            });
            
            if (user.IsDeleted) return BadRequest(new ErrorResponse
            {
                Errors = new List<ErrorModel>
                {
                    new ErrorModel
                    {
                        FieldName = "Id",
                        Message = "User has already been deleted"
                    }
                }
            });
            
            
            //не баг а фича =))
            user.Country = request.Country;
            user.FirstName = request.FirstName;
            user.SecondName = request.SecondName;
            user.DateOfBirth = request.DateOfBirth;
            user.Gender = request.Gender;
            
            
            var result = await _dataManager.UserRepository.UpdateUserAsync(user);
            await _dataManager.SaveAsync();
            
            var publishMessage = _mapper.Map<UserEdited>(user);
            await _publishEndpoint.Publish(publishMessage);

            return Ok();
        }
        
        
        /// <summary>
        /// Get all user
        /// </summary>
        /// <param name="pageSize">Number of objects per page. Default 20. Max 50.</param>
        /// <param name="pageNumber">Page number.</param>
        /// <response code = "200"> Successful request </response>
        /// <response code = "204"> successful request, Id not found </response>
        /// <response code = "400"> Invalid arguments</response>
        [HttpGet]
        [Route(ApiRoutes.User.Get)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetAllUser([FromQuery]int pageSize=20, [FromQuery]int pageNumber=1)
        {
            var users = await _dataManager.UserRepository.GetAllUserAsync(pageSize, pageNumber);
            if (users == null) return NoContent();
                
            var mapped = _mapper.Map<List<UserResponse>>(users);
            return Ok(mapped);
        }
        
        /// <summary>
        /// Get user by Id
        /// </summary>
        /// <param name="id"> User id</param>
        /// <response code = "200"> Successful request </response>
        /// <response code = "204"> successful request, Id not found </response>
        /// <response code = "400"> Invalid arguments</response>
        [HttpGet]
        [Route(ApiRoutes.User.GetById)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetById([FromQuery]Guid id)
        {
            if (id == Guid.Empty) return BadRequest(new ErrorResponse
            {
                Errors = new List<ErrorModel>{new ErrorModel
                {
                    FieldName = "Id",
                    Message = "Cannot be empty"
                }}
            });
            
            var user = await _dataManager.UserRepository.GetUserByIdAsync(id);
            if (user == null) return NoContent();
                
            var mapped = _mapper.Map<UserResponse>(user);
            return Ok(mapped);
        }
        
        /// <summary>
        /// Get user by Email
        /// </summary>
        /// <param name="email">Email user</param>
        /// <response code = "200"> Successful request </response>
        /// <response code = "204"> successful request, Email not found </response>
        /// <response code = "400"> Invalid arguments</response>
        [HttpGet]
        [Route(ApiRoutes.User.GetByEmail)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetByEmail([FromQuery]string email)
        {
            if (string.IsNullOrEmpty(email)) return BadRequest(new ErrorResponse
            {
                Errors = new List<ErrorModel>{new ErrorModel
                {
                    FieldName = "Email",
                    Message = "Cannot be empty"
                }}
            });
            
            var user = await _dataManager.UserRepository.GetUserByEmailAsync(email);
            if (user == null) return NoContent();
                
            var mapped = _mapper.Map<UserResponse>(user);
            return Ok(mapped);
        }
        
        
        /// <summary>
        /// Remove user
        /// </summary>
        /// <param name="id">Id user</param>
        /// <response code = "200"> Successful request </response>
        /// <response code = "204"> successful request, Id not found </response>
        /// <response code = "400"> Invalid arguments</response>
        [HttpDelete]
        [Route(ApiRoutes.User.Remove)]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> RemoveUser([FromQuery]Guid id)
        {
            if (id == Guid.Empty) return BadRequest(new ErrorResponse
            {
                Errors = new List<ErrorModel>{new ErrorModel
                {
                    FieldName = "Id",
                    Message = "Cannot be empty"
                }}
            });
            
            var user = await _dataManager.UserRepository.GetUserByIdAsync(id);
            
            if (user == null) return BadRequest(new ErrorResponse
            {
                Errors = new List<ErrorModel>{new ErrorModel
                {
                    FieldName = "Id",
                    Message = "User does not exist"
                }}
            });
            
            if (user.IsDeleted) return BadRequest(new ErrorResponse
            {
                Errors = new List<ErrorModel>{new ErrorModel
                {
                    FieldName = "Id",
                    Message = "User has already been deleted"
                }}
            });
            
            
            var result = await _dataManager.UserRepository.RemoveUserAsync(id);
            
            
            await _publishEndpoint.Publish(new UserDeleted{Id = id});
                
            return NoContent();
        }
        
        
        
    }
}