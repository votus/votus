﻿using System.Net.Http;
using System.Security.Authentication;
using AutoMapper;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using VOtus.UserInfoService.BL.UnitOfWork;
using VOtus.UserInfoService.Web.Filters;

namespace VOtus.UserInfoService.Web.ConfigureService
{
    public class ConfigureServiceMvc
    {
        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            services.AddCors();
            // services.AddAuthentication(options =>
            //     {
            //         options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //         options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            //     })
            //     .AddJwtBearer(config =>
            //     {
            //         config.Authority = "server auth";
            //         config.Audience = "WebAPI";
            //         config.BackchannelHttpHandler = GetHandler();
            //     });
            //
            services.AddMvc(op =>
                {
                    op.EnableEndpointRouting = false;
                    op.Filters.Add<ValidationFilter>();
                })
                .AddFluentValidation(fv =>
                {
                    fv.RegisterValidatorsFromAssemblyContaining<Startup>();
                });
            
            services.AddTransient(typeof(IDataManager), typeof(DataManager));
            services.AddAutoMapper(typeof(Startup).Assembly);
            
            services.AddControllers();
        }
        
        
        private static HttpClientHandler GetHandler()
        {
            var handler = new HttpClientHandler
            {
                ClientCertificateOptions = ClientCertificateOption.Manual,
                SslProtocols = SslProtocols.Tls12,
                ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true
            };
        
            return handler;
        }
    }
}