﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using VOtus.UserInfoService.DAL;

namespace VOtus.UserInfoService.Web.ConfigureService
{
    public class ConfigureServiceDatabase
    {
        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            
            if (string.IsNullOrEmpty(Settings.Database.ConnectionString))
            {
                throw new NullReferenceException("Connection string does not empty");
            }
            
            services.AddDbContext<DataContext>(x =>
            {
                x.UseNpgsql(Settings.Database.ConnectionString);
                x.UseLazyLoadingProxies();
            });
        }
    }
}


