#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

#Depending on the operating system of the host machines(s) that will build or run the containers, the image specified in the FROM statement may need to be changed.
#For more information, please see https://aka.ms/containercompat

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-nanoserver-1903 AS base
ENV DOTNET_USE_POLLING_FILE_WATCHER 1
ENV ASPNETCORE_ENVIRONMENT Development
ENV ASPNETCORE_URLS "http://+:8001"
WORKDIR /app
EXPOSE 8000

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-nanoserver-1903 AS build
WORKDIR /src
COPY ["VOtus.UserInfoService/src/VOtus.UserInfoService.Web/VOtus.UserInfoService.Web.csproj", "VOtus.UserInfoService/src/VOtus.UserInfoService.Web/"]
COPY ["VOtus.BrokerContracts/src/VOtus.BrokerContracts.csproj", "VOtus.BrokerContracts/src/"]
COPY ["VOtus.UserInfoService/src/VOtus.UserInfoService.DAL/VOtus.UserInfoService.DAL.csproj", "VOtus.UserInfoService/src/VOtus.UserInfoService.DAL/"]
COPY ["VOtus.UserInfoService/src/VOtus.UserInfoService.BL/VOtus.UserInfoService.BL.csproj", "VOtus.UserInfoService/src/VOtus.UserInfoService.BL/"]
RUN dotnet restore "VOtus.UserInfoService/src/VOtus.UserInfoService.Web/VOtus.UserInfoService.Web.csproj"
COPY . .
WORKDIR "/src/VOtus.UserInfoService/src/VOtus.UserInfoService.Web"
RUN dotnet build "VOtus.UserInfoService.Web.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "VOtus.UserInfoService.Web.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "VOtus.UserInfoService.Web.dll"]