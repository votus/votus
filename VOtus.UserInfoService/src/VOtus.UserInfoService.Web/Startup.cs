using System;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using VOtus.UserInfoService.BL.UnitOfWork;
using VOtus.UserInfoService.DAL;
using VOtus.UserInfoService.Web.ConfigureService;
using VOtus.UserInfoService.Web.Filters;

namespace VOtus.UserInfoService.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        
        
        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureServiceDatabase.Configure(services, Configuration);
            ConfigureServiceMassTransit.Configure(services, Configuration);
            ConfigureServiceSwagger.Configure(services, Configuration);
            ConfigureServiceMvc.Configure(services, Configuration);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(op =>
            {
                op.AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowAnyOrigin();
            });
            
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "UserInfoService v1"));
            
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}