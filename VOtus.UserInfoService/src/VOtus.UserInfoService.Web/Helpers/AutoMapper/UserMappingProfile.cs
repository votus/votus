﻿using AutoMapper;
using BrokerContracts.User;
using VOtus.UserInfoService.DAL.Domain;
using VOtus.UserInfoService.Web.Contracts.Requests;
using VOtus.UserInfoService.Web.Contracts.Response;

namespace VOtus.UserInfoService.Web.Helpers.AutoMapper
{
    public class UserMappingProfile : Profile
    {
        public UserMappingProfile()
        {
            CreateMap<User, UserCreated>()
                .ReverseMap();

            CreateMap<User, UserEdited>()
                .ReverseMap();
            
            CreateMap<User, UserResponse>()
                .ReverseMap();
            
            CreateMap<User, UpdateUserRequest>()
                .ReverseMap();
        }
    }
}