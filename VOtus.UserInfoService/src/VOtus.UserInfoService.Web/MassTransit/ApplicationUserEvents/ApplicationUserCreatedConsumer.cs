﻿using System.Threading.Tasks;
using AutoMapper;
using BrokerContracts.User;
using MassTransit;
using Microsoft.Extensions.Logging;
using VOtus.UserInfoService.BL.UnitOfWork;
using VOtus.UserInfoService.DAL.Domain;

namespace VOtus.UserInfoService.Web.MassTransit.ApplicationUserEvents
{
    public class ApplicationUserCreatedConsumer : IConsumer<UserCreated>
    {
        private readonly IDataManager _dataManager;
        private readonly ILogger<ApplicationUserCreatedConsumer> _logger;
        private readonly IMapper _mapper;

        public ApplicationUserCreatedConsumer(IDataManager dataManager, IMapper mapper,
            ILogger<ApplicationUserCreatedConsumer> logger)
        {
            _dataManager = dataManager;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<UserCreated> context)
        {
            var model = _mapper.Map<User>(context.Message);
            _logger.LogInformation("Get message from broker. Create user: {@User}", model);
            await _dataManager.UserRepository.CreateUserAsync(model);
        }

    }
}