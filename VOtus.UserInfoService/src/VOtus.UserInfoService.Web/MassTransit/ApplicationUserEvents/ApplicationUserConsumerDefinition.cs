﻿using BrokerContracts;
using MassTransit.Definition;


namespace VOtus.UserInfoService.Web.MassTransit.ApplicationUserEvents
{
    public class ApplicationUserConsumerDefinition
    {
        public class ApplicationUserCreatedConsumerDefinition : ConsumerDefinition<ApplicationUserCreatedConsumer>
        {
            public ApplicationUserCreatedConsumerDefinition()
            {
                EndpointName = Constants.NotificationQueueCore;
            }
        }
        
    }
}