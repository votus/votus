﻿using System;
using VOtus.UserInfoService.DAL.Helpers;

namespace VOtus.UserInfoService.Web.Contracts.Requests
{
    public class UpdateUserRequest
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public Gender Gender { get; set; }
        public string Country { get; set; }
    }
}