﻿using Microsoft.AspNetCore.Server.Kestrel.Https;

namespace VOtus.UserInfoService.Web.Contracts.Routes
{
    public static class ApiRoutes
    {
        private const string Root = "api";
        private const string Version = "v1";
        private const string Base = Root + "/" + Version;

        public static class User
        {
            public const string Register = Base + "/[controller]/register";
            public const string Update = Base + "/[controller]";
            public const string Remove = Base + "/[controller]";
            public const string Get = Base + "/[controller]";
            public const string GetById = Base + "/[controller]/id";
            public const string GetByEmail = Base + "/[controller]/email";
        }

        public static class Friend
        {
            public const string AddToFriends = Base + "/[controller]";
            public const string GetFriends = Base + "/[controller]";
            public const string RemoveFromFriends = Base + "/[controller]";
        }

        public static class BlackList
        {
            public const string AddToBlackList = Base + "/[controller]/addtbl";
            public const string RemoveFromBlackList = Base + "/[controller]/rfbl";
        }
    }
}