﻿using System;
using System.Text.RegularExpressions;
using FluentValidation;
using VOtus.UserInfoService.Web.Contracts.Requests;

namespace VOtus.UserInfoService.Web.Validators
{
    public class RegisterUserRequestValidator : AbstractValidator<RegisterUserRequest>
    {
        public RegisterUserRequestValidator()
        {
            RuleFor(x => x.FirstName)
                .NotEmpty()
                .NotNull()
                .MinimumLength(3)
                .MaximumLength(30)
                .WithMessage("Field FirstName does not empty");
            
            RuleFor(x => x.SecondName)
                .NotEmpty()
                .NotNull()
                .MinimumLength(3)
                .MaximumLength(30)
                .WithMessage("Field SecondName does not empty");
            
            RuleFor(x => x.Country)
                .NotEmpty()
                .NotNull()
                .MinimumLength(3)
                .MaximumLength(30)
                .WithMessage("Field Country does not empty");

            RuleFor(x => x.Email)
                .EmailAddress()
                .WithMessage("Field Email is not valid");
        }


    }
}