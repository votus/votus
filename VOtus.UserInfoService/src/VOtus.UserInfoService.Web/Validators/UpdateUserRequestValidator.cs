﻿using System;
using FluentValidation;
using VOtus.UserInfoService.Web.Contracts.Requests;

namespace VOtus.UserInfoService.Web.Validators
{
    public class UpdateUserRequestValidator: AbstractValidator<UpdateUserRequest>
    {
        public UpdateUserRequestValidator()
        {
            RuleFor(x => x.Id)
                .NotEqual(Guid.Empty)
                .WithMessage("Field Id does not empty");
            
            RuleFor(x => x.FirstName)
                .NotEmpty()
                .NotNull()
                .MinimumLength(3)
                .MaximumLength(30)
                .WithMessage("Field FirstName does not empty");
            
            RuleFor(x => x.SecondName)
                .NotEmpty()
                .NotNull()
                .MinimumLength(3)
                .MaximumLength(30)
                .WithMessage("Field SecondName does not empty");
            
            RuleFor(x => x.Country)
                .NotEmpty()
                .NotNull()
                .MinimumLength(3)
                .MaximumLength(30)
                .WithMessage("Field Country does not empty");
            
            RuleFor(x => x.DateOfBirth)
                .NotEqual(DateTime.MinValue)
                .WithMessage("Field Country does not empty");
        }


    }
}