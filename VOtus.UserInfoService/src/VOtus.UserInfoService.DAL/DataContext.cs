﻿using Microsoft.EntityFrameworkCore;
using VOtus.UserInfoService.DAL.Domain;

namespace VOtus.UserInfoService.DAL
{
    public class DataContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Friends> Friends { get; set; }
        public DbSet<BlackList> BlackList { get; set; }

        public DataContext(): base()
        {
        }
        
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BlackList>()
                .HasOne(u => u.User)
                .WithMany(b => b.BlackList)
                .HasForeignKey(k=>k.UserId);
            
            modelBuilder.Entity<Friends>()
                .HasOne(u => u.User)
                .WithMany(b => b.Friends)
                .HasForeignKey(k=>k.UserId);

            modelBuilder.Entity<User>()
                .Property(p => p.FirstName)
                .IsRequired()
                .HasMaxLength(30);
            
            modelBuilder.Entity<User>()
                .Property(p => p.SecondName)
                .IsRequired()
                .HasMaxLength(30);
            modelBuilder.Entity<User>()
                .Property(p => p.Email)
                .IsRequired()
                .HasMaxLength(50);
            modelBuilder.Entity<User>()
                .Property(p => p.Country)
                .IsRequired()
                .HasMaxLength(30);
            modelBuilder.Entity<User>()
                .Property(p => p.CreatedAt)
                .IsRequired();
            modelBuilder.Entity<User>()
                .Property(p => p.DateOfBirth)
                .IsRequired();
        }
    }
}