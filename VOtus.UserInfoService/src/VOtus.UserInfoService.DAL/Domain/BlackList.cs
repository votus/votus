﻿using System;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace VOtus.UserInfoService.DAL.Domain
{
    public class BlackList : BaseEntity
    {
        public Guid UserId { get; set; }
        public virtual User User { get; set; }
        public virtual User Friend { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}