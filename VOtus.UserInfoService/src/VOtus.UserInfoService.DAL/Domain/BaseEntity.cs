﻿using System;

namespace VOtus.UserInfoService.DAL.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}