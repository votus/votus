﻿using System;

namespace VOtus.UserInfoService.DAL.Domain
{
    public class Friends : BaseEntity
    {
        public Guid UserId { get; set; }
        public virtual User User { get; set; }
        public virtual User Friend { get; set; }
        public DateTime? CreatedAt { get; set; }
        public bool IsApproval { get; set; }
        public DateTime? ApprovalDate { get; set; }
    }
}