﻿using System;
using System.Collections;
using System.Collections.Generic;
using VOtus.UserInfoService.DAL.Helpers;

namespace VOtus.UserInfoService.DAL.Domain
{
    public class User : BaseEntity
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Email { get; set; }
        public Gender Gender { get; set; }
        public string Country { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public bool IsDeleted { get; set; }
        public virtual ICollection<Friends> Friends { get; set; }
        public virtual ICollection<BlackList> BlackList { get; set; }
    }
}