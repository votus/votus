﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VOtus.IdentityServer.Models.Enums;

namespace VOtus.IdentityServer.Models
{
	public class UserCreateModel
	{
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public Gender Gender { get; set; }
        public string Country { get; set; }
    }
}
