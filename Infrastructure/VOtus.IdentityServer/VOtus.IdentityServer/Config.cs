﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;

namespace VOtus.IdentityServer
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            { 
                new IdentityResources.OpenId(),
                new IdentityResource("userId", new string [] {"userId"})
            };

        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            {
                new ApiScope("userInfoAPI"," VOtus UserInfo", new string[] {"userId"}),
                new ApiScope("newsAPI"," VOtus News", new string[] {"userId"}),
                new ApiScope("groupAPI"," VOtus Groups", new string[] {"userId"})
            };

        public static IEnumerable<ApiResource> GetApis()
        {
            return new List<ApiResource>
            {
                new ApiResource("api", "API")
                {
                    ApiSecrets = { new Secret("secret".Sha256()) },

                    Scopes = { "userInfoAPI", "newsAPI", "groupAPI" }
                }
            };
        }

        public static IEnumerable<Client> Clients =>
            new Client[] 
            {
                new Client()
                {
                     ClientId = "front",
                     AllowedGrantTypes = GrantTypes.ClientCredentials,
                     ClientSecrets =
                     {
                     new Secret("secret".Sha256())
                     },
                    AllowedScopes = { 
                        "userInfoAPI", 
                        "newsAPI",
                        "groupAPI",
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                    },
                    AllowAccessTokensViaBrowser = true
                }
            };

    }
}