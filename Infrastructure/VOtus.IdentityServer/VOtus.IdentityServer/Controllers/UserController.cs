﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using VOtus.IdentityServer.Abstractions.Repositories;
using VOtus.IdentityServer.Models;
using VOtus.IdentityServer.Models.Domain;

namespace VOtus.IdentityServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IRepository<User> _userRepository;

        private readonly ILogger _logger;

        public UserController(IRepository<User> userRepository, ILogger logger)
        {
            _userRepository = userRepository;
            _logger = logger;
        }

        [HttpPost]
        [ProducesResponseType(400)]
        public async Task<ActionResult> CreateUser([FromBody]UserCreateModel model)
        {
            var user = new User();
            await _userRepository.AddAsync(user);

            return Ok();
        }
    }
}