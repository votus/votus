﻿using Microsoft.EntityFrameworkCore;
using VOtus.IdentityServer.Models.Domain;

namespace VOtus.IdentityServer.DAL
{
	public class DataContext : DbContext
	{
        public DbSet<User> Users { get; set; }

        public DataContext() : base()
        {
        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .Property(p => p.FirstName)
                .IsRequired()
                .HasMaxLength(30);

            modelBuilder.Entity<User>()
                .Property(p => p.SecondName)
                .IsRequired()
                .HasMaxLength(30);
            modelBuilder.Entity<User>()
                .Property(p => p.Email)
                .IsRequired()
                .HasMaxLength(50);
            modelBuilder.Entity<User>()
                .Property(p => p.Country)
                .IsRequired()
                .HasMaxLength(30);
            modelBuilder.Entity<User>()
                .Property(p => p.CreatedAt)
                .IsRequired();
            modelBuilder.Entity<User>()
                .Property(p => p.DateOfBirth)
                .IsRequired();
            modelBuilder.Entity<User>()
                .Property(p => p.Password)
                .IsRequired();
        }
    }
}
