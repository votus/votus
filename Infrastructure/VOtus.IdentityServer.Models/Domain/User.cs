﻿using System;
using System.Collections.Generic;
using System.Text;
using VOtus.IdentityServer.Models.Domain.Enums;

namespace VOtus.IdentityServer.Models.Domain
{
	public class User : BaseEntity
	{
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Email { get; set; }
        public Gender Gender { get; set; }
        public string Country { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public bool IsDeleted { get; set; }
        public string Password { get; set; }
    }
}
