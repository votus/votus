﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VOtus.IdentityServer.Models.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
