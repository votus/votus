﻿using System.Collections.Generic;

namespace VOtus.GroupService.Core.Domain
{
    public class Group: BaseEntity
    {
        public string Name { get; set; }
        public virtual ICollection<GroupUser> Users { get; set; }
    }
}
