﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace VOtus.GroupService.Core.Domain
{
    public class User : BaseEntity
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string DateOfBirth { get; set; }

        public string Email { get; set; }

        public int Gender { get; set; }

        public string Country { get; set; }
        public virtual ICollection<GroupUser> Groups { get; set; }

    }
}
