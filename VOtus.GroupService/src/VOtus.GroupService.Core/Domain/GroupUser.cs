﻿using System;

namespace VOtus.GroupService.Core.Domain
{
    public class GroupUser
    {
        public Guid GroupId { get; set; }
        public virtual Group Group { get; set; }
        public Guid UserId { get; set; }
        public virtual User User { get; set; }
        
    }
}
