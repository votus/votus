﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VOtus.GroupService.Core.Abstractions
{
	/// <summary>
	/// Класс для оборачивания запросов с листами
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class PaginatedList<T>
	{
		/// <summary>
		/// Конструктор
		/// </summary>
		public PaginatedList(IEnumerable<T> items, int pageIndex, int totalPages, int totalItems)
		{
			PageIndex = pageIndex;
			TotalPages = totalPages;
			Items = items;
			TotalItems = totalItems;
		}
		/// <summary>
		/// Номер текущей страницы
		/// </summary>
		public int PageIndex { get; set; }
		/// <summary>
		/// Всего страниц
		/// </summary>
		public int TotalPages { get; set; }
		/// <summary>
		/// Всего элементов в исходной коллекции
		/// </summary>
		public int TotalItems { get; set; }

		/// <summary>
		/// Коллекция элементов
		/// </summary>
		public IEnumerable<T> Items { get; set; }


		/// <summary>
		/// Метод по созданию страницы
		/// </summary>
		public static PaginatedList<T> Create(IQueryable<T> source, int pageIndex, int pageSize)
		{
			var count = source.Count();
			var items = source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
			var totalPages = 0;
			if (count != 0)
				totalPages = (int)Math.Ceiling(count / (double)pageSize);

			return new PaginatedList<T>(items, pageIndex, totalPages, count);
		}
		/// <summary>
		/// Метод по созданию страницы
		/// </summary>
		public static PaginatedList<T> Create(IEnumerable<T> source, int pageIndex, int pageSize)
		{
			var count = source.Count();
			var items = source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
			var totalPages = 0;
			if (count != 0)
				totalPages = (int)Math.Ceiling(count / (double)pageSize);

			return new PaginatedList<T>(items, pageIndex, totalPages, count);
		}
	}
}
