﻿using System;

namespace VOtus.GroupService.Core
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsClosed { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public BaseEntity()
        {
            Id = Guid.NewGuid();
            IsClosed = false;
            CreateDate = DateTime.UtcNow;
            LastModifiedDate = DateTime.UtcNow;
        }
    }
}
