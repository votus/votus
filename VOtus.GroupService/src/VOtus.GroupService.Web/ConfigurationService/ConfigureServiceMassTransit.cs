﻿using Microsoft.Extensions.DependencyInjection;
using MassTransit;
using MassTransit.Definition;
using VOtus.GroupService.MassTransit.ApplicationEvents;

namespace VOtus.GroupService.ConfigurationService
{
    public class ConfigureServiceMassTransit
    {
        public static void Configure(IServiceCollection services)
        {
            services.AddMassTransit(x =>
            {
                x.AddBus(busFactory =>
                {
                    var bus = Bus.Factory.CreateUsingRabbitMq(cfg =>
                    {
                        cfg.Host(Settings.MassTransit.Url, 5672, Settings.MassTransit.Host, configurator =>
                        {
                            configurator.Username(Settings.MassTransit.UserName);
                            configurator.Password(Settings.MassTransit.Password);
                        });

                        cfg.ConfigureEndpoints(busFactory, KebabCaseEndpointNameFormatter.Instance);

                        cfg.UseJsonSerializer();

                        cfg.UseHealthCheck(busFactory);
                    });

                    return bus;
                });

                x.AddConsumer<ApplicationUserCreatedConsumer>(typeof(ApplicationUserConsumerDefinition.ApplicationUserCreatedConsumerDefinition));
                x.AddConsumer<ApplicationUserDeletedConsumer>(typeof(ApplicationUserConsumerDefinition.ApplicationUserDeletedConsumerDefinition));
                x.AddConsumer<ApplicationUserEditedConsumer>(typeof(ApplicationUserConsumerDefinition.ApplicationUserEditedConsumerDefinition));
            });
            services.AddMassTransitHostedService();
        }
    }
}
