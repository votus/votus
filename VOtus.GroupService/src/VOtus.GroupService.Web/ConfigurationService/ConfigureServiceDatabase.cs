﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using VOtus.GroupService.DataAccess;

namespace VOtus.GroupService.ConfigurationService
{
    public class ConfigureServiceDatabase
    {
        //private readonly IConfiguration _configuration;
        public static void Configure(IServiceCollection services, IConfiguration _configuration)
        {
            var name = typeof(Database).Assembly.GetName().Name;
            if (string.IsNullOrEmpty(Settings.Database.ConnectionString))
            {
                throw new ArgumentNullException("Connection string does not empty");
            }

            services.AddDbContext<DataContext>(x =>
            {
                x.UseNpgsql(_configuration.GetConnectionString(Settings.Database.ConnectionString), assembly => assembly.MigrationsAssembly(Settings.Database.MigrationsAssemblyName));
                x.UseLazyLoadingProxies();
            });
        }
    }
}
