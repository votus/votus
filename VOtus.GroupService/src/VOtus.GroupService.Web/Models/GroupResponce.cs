﻿namespace VOtus.GroupService.Models
{
    /// <summary>
    /// Класс ответа запроса группы
    /// </summary>
    public class GroupResponce
    {
        /// <summary>
        /// Имя группы
        /// </summary>
        public string Name { get; set; }
    }
}
