﻿namespace VOtus.GroupService.Models
{
    /// <summary>
    /// Класс ответа запроса пользователей группы
    /// </summary>
    public class GroupUsersResponce
    {
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string SecondName { get; set; }
        /// <summary>
        /// Ник
        /// </summary>
        public string NickName { get; set; }
    }
}
