﻿using System;

namespace VOtus.GroupService
{
    public static class Settings
    {
        private static readonly string _env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
        public static class MassTransit
        {
            public static readonly string Url = Environment.GetEnvironmentVariable("MASSTRANSIT_URL");
            public static readonly string Host = Environment.GetEnvironmentVariable("MASSTRANSIT_HOST");
            public static readonly string UserName = Environment.GetEnvironmentVariable("MASSTRANSIT_USERNAME");
            public static readonly string Password = Environment.GetEnvironmentVariable("MASSTRANSIT_PASSWORD");
        }

        public static class Database
        {
            public static readonly string ConnectionString = "DataContextString";
            public static readonly string MigrationsAssemblyName = "VOtus.GroupService.DataAccess";
        }

        public static class Enviroment
        {
            public static bool IsProduction => _env == "Production";
            public static bool IsDevelopment => _env == "Development";
        }
    }
}
