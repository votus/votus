﻿using System;
using System.Threading.Tasks;
using BrokerContracts.User;
using MassTransit;
using Microsoft.Extensions.Logging;
using VOtus.GroupService.Core.Abstractions.Repositories;
using VOtus.GroupService.Core.Domain;

namespace VOtus.GroupService.MassTransit.ApplicationEvents
{
    public class ApplicationUserEditedConsumer : IConsumer<UserEdited>
    {
        private readonly IRepository<User> _userRepository;
        private readonly ILogger<ApplicationUserEditedConsumer> _logger;

        public ApplicationUserEditedConsumer(IRepository<User> userRepository, ILogger<ApplicationUserEditedConsumer> logger)
        {
            _userRepository = userRepository;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<UserEdited> context)
        {
            _logger.LogInformation("Get message from broker. Create user: {@User}", context.Message);
            var userToUpdate = await _userRepository.GetByIdAsync(context.Message.Id);
            userToUpdate.FirstName = context.Message.FirstName;
            userToUpdate.SecondName = context.Message.SecondName;
            userToUpdate.DateOfBirth = context.Message.DateOfBirth;
            userToUpdate.Email = context.Message.Email;
            userToUpdate.Gender = context.Message.Gender;
            userToUpdate.Country = context.Message.Country;
            userToUpdate.LastModifiedDate = DateTime.UtcNow;

            await _userRepository.UpdateAsync(userToUpdate);
        }

    }
}