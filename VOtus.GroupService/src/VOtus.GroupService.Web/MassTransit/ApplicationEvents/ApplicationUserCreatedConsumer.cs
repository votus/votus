﻿using System.Threading.Tasks;
using AutoMapper;
using BrokerContracts.User;
using MassTransit;
using Microsoft.Extensions.Logging;
using VOtus.GroupService.Core.Abstractions.Repositories;
using VOtus.GroupService.Core.Domain;

namespace VOtus.GroupService.MassTransit.ApplicationEvents
{
    public class ApplicationUserCreatedConsumer : IConsumer<UserCreated>
    {
        private readonly IRepository<User> _userRepository;
        private readonly ILogger<ApplicationUserCreatedConsumer> _logger;
        private readonly IMapper _mapper;

        public ApplicationUserCreatedConsumer(IRepository<User> userRepository, IMapper mapper,
            ILogger<ApplicationUserCreatedConsumer> logger)
        {
            _userRepository = userRepository;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<UserCreated> context)
        {
            var model = _mapper.Map<User>(context.Message);
            _logger.LogInformation("Get message from broker. Create user: {@User}", model);
            await _userRepository.AddAsync(model);
        }

    }
}