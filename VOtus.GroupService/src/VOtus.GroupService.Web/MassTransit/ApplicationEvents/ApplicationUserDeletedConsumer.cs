﻿using System.Threading.Tasks;
using AutoMapper;
using BrokerContracts.User;
using MassTransit;
using Microsoft.Extensions.Logging;
using VOtus.GroupService.Core.Abstractions.Repositories;
using VOtus.GroupService.Core.Domain;

namespace VOtus.GroupService.MassTransit.ApplicationEvents
{
    public class ApplicationUserDeletedConsumer : IConsumer<UserDeleted>
    {
        private readonly IRepository<User> _userRepository;
        private readonly ILogger<ApplicationUserDeletedConsumer> _logger;
        private readonly IMapper _mapper;

        public ApplicationUserDeletedConsumer(IRepository<User> userRepository, IMapper mapper,
            ILogger<ApplicationUserDeletedConsumer> logger)
        {
            _userRepository = userRepository;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<UserDeleted> context)
        {
            var model = _mapper.Map<User>(context.Message);
            _logger.LogInformation("Get message from broker. Delete user: {@User}", model);
            var userToUpdate = await _userRepository.GetByIdAsync(context.Message.Id);
            if(userToUpdate != null)
            {
                userToUpdate.IsClosed = true;
                await _userRepository.UpdateAsync(userToUpdate);
            }
            else
            {
                _logger.LogError("User not found", model);
            }
            
        }

    }
}