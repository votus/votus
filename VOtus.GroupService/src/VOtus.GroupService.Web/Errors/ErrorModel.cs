﻿using System.Collections.Generic;

namespace VOtus.GroupService.Errors
{
    public class ErrorModel
    {
        public string FieldName { get; set; }

        public string Message { get; set; }
    }

    public class ErrorResponse
    {
        public ErrorResponse() { }

        public ErrorResponse(ErrorModel error)
        {
            Errors.Add(error);
        }

        public List<ErrorModel> Errors { get; set; } = new List<ErrorModel>();
    }
}
