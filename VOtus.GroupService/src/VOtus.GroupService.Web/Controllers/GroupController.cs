﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using BrokerContracts.Group;
using VOtus.GroupService.Core.Abstractions;
using VOtus.GroupService.Core.Abstractions.Repositories;
using VOtus.GroupService.Core.Domain;
using VOtus.GroupService.Errors;
using VOtus.GroupService.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VOtus.GroupService.Controllers
{

    /// <summary>
    /// Контроллер работы с группами
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class GroupController
        : ControllerBase
    {
        private readonly IRepository<Group> _groupRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IPublishEndpoint _publishEndpoint;
        private readonly IMapper _mapper;


        public GroupController(IRepository<Group> groupRepository, IRepository<User> userRepository, IMapper mapper,
             IPublishEndpoint publishEndpoint)
        {
            _groupRepository = groupRepository;
            _userRepository = userRepository;
            _mapper = mapper;
            _publishEndpoint = publishEndpoint;
        }

        /// <summary>
        /// Получение всех групп
        /// </summary>
        ///  <remarks>    
        ///  
        ///         GET: api/Group
        ///         
        /// </remarks>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GroupResponce>>> GetAll(Guid? userId, int pageIndex = 1, int pageSize = 10)
        {

            var groups = userId.HasValue ? await _groupRepository.GetWhere(x => x.Users.Any(user => user.User.Id == userId.Value)) : await _groupRepository.GetAllAsync();
            var groupsResponce = groups.Select(group => new GroupResponce { Name = group.Name }).OrderBy(item => item.Name);
            var page = PaginatedList<GroupResponce>.Create(groupsResponce, pageIndex, pageSize);

            return Ok(page.Items);
        }

        /// <summary>
        /// Получение одной группы
        /// </summary>
        /// <remarks>
        /// 
        ///     GET api/Group/f30ac938-c8ad-46ce-8aa9-45103ce4b0c4
        /// 
        /// </remarks>
        /// <param name="groupId">Уникальный идентификатор группы</param>
        /// <returns></returns>
        [HttpGet("{groupId}")]
        public ActionResult<Group> Get(Guid groupId)
        {
            var group = GetGroup(groupId, out var errorResponse);
            if (errorResponse != null)
            {
                return BadRequest(errorResponse);
            }
            return Ok(new GroupResponce() { Name = group.Name });
        }

        /// <summary>
        /// Создание группы
        /// </summary>
        /// <remarks>
        /// 
        ///     Post: api/Group
        ///     
        /// </remarks>
        /// <param name="groupName">Имя группы</param>
        [HttpPost("{groupName}")]
        public async Task<ActionResult> Create(string groupName)
        {
            var group = new Group
            {
                Id = Guid.NewGuid(),
                CreateDate = DateTime.UtcNow,
                IsClosed = false,
                LastModifiedDate = DateTime.UtcNow,
                Name = groupName
            };
            await _groupRepository.AddAsync(group);
            await _publishEndpoint.Publish(_mapper.Map<Group, GroupCreated>(group));
            return Ok();
        }

        /// <summary>
        /// Добавить пользователей в группу
        /// </summary>
        /// <remarks>
        /// 
        ///     Patch: api/Group/f30ac938-c8ad-46ce-8aa9-45103ce4b0c4/AddUsers
        ///     
        /// </remarks>
        /// <param name="groupId">Идентификатор группы</param>
        /// <param name="userIds">Список идентификаторов пользователя</param>
        [HttpPatch("{groupId}/AddUsers")]
        public async Task<ActionResult> AddUsers(Guid groupId, [FromBody] Guid[] userIds)
        {
            var group = GetGroup(groupId, out var errorResponse);
            if (errorResponse != null)
            {
                return BadRequest(errorResponse);
            }

            var users = await _userRepository.GetWhere(x => userIds.Contains(x.Id));
            var ids = userIds.Intersect(users.Select(x => x.Id));
            var groupUsers = group.Users.Select(x => x.UserId);
            var listUserIdsAdd = ids.Except(groupUsers);
            var usersAdd = listUserIdsAdd.Select(x => new GroupUser { GroupId = group.Id, UserId = x });
            usersAdd.ToList().ForEach(item => group.Users.Add(item));
            await _groupRepository.UpdateAsync(group);

            return Ok();
        }



        /// <summary>
        /// Добавить пользователя в группу
        /// </summary>
        /// <remarks>
        /// 
        ///     Patch: api/Group/f30ac938-c8ad-46ce-8aa9-45103ce4b0c4/AddUser/c30ac938-c8ad-46ce-8aa9-45103ce4b0ck
        ///     
        /// </remarks>
        /// <param name="groupId">Идентификатор группы</param>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <response code = "400"> Не корректный ввод аргументов</response>
        [HttpPatch("{groupId}/AddUser/{userId}")]
        [ProducesResponseType(400)]
        public async Task<ActionResult> AddUser(Guid groupId, Guid userId)
        {
            var group = GetGroup(groupId, out var errorResponse);
            if (errorResponse != null)
            {
                return BadRequest(errorResponse);
            }

            var user = GetUser(userId, out var errorUserResponse);
            if (errorUserResponse != null)
            {
                return BadRequest(errorUserResponse);
            }

            if (!group.Users.Any(x => x.UserId == user.Id))
            {
                group.Users.Add(new GroupUser { GroupId = group.Id, UserId = user.Id });
                await _groupRepository.UpdateAsync(group);
            }

            return Ok();
        }

        /// <summary>
        /// Исключить пользователей из группы
        /// </summary>
        /// <remarks>
        /// 
        ///     Patch: api/Group/f30ac938-c8ad-46ce-8aa9-45103ce4b0c4/RemoveUsers
        ///     
        /// </remarks>
        /// <param name="groupId">Идентификатор группы</param>
        /// <param name="userIds">Список идентификаторов пользователя</param>
        [HttpPatch("{groupId}/RemoveUsers")]
        [ProducesResponseType(400)]
        public async Task<ActionResult> RemoveUsers(Guid groupId, [FromBody] Guid[] userIds)
        {
            var group = GetGroup(groupId, out var errorResponse);
            if (errorResponse != null)
            {
                return BadRequest(errorResponse);
            }
            foreach (var id in userIds)
            {
                var user = await _userRepository.GetByIdAsync(id);
                group.Users.Remove(group.Users.Any(x => x.UserId == user.Id) ? group.Users.First(x => x.UserId == user.Id) : null);
                await _groupRepository.UpdateAsync(group);
            }

            return Ok();
        }

        /// <summary>
        /// Исключить пользователя из группы
        /// </summary>
        /// <remarks>
        /// 
        ///     Patch: api/Group/f30ac938-c8ad-46ce-8aa9-45103ce4b0c4/RemoveUser/c30ac938-c8ad-46ce-8aa9-45103ce4b0ck
        ///     
        /// </remarks>
        /// <param name="groupId">Идентификатор группы</param>
        /// <param name="userId">Идентификатор пользователя</param>
        [HttpPatch("{groupId}/RemoveUser/{userId}")]
        public async Task<ActionResult> RemoveUser(Guid groupId, Guid userId)
        {
            var group = GetGroup(groupId, out var errorResponse);
            if (errorResponse != null)
            {
                return BadRequest(errorResponse);
            }
            var user = await _userRepository.GetByIdAsync(userId);
            group.Users.Remove(group.Users.Any(x => x.UserId == user.Id) ? group.Users.First(x => x.UserId == user.Id) : null);
            await _groupRepository.UpdateAsync(group);

            return Ok();
        }


        /// <summary>
        /// Получить список пользователей группы
        /// </summary>
        /// <remarks>
        /// 
        ///     Get: api/Group/f30ac938-c8ad-46ce-8aa9-45103ce4b0c4
        ///     {
        ///         pageIndex : "1",
        ///         pageSize : "10"
        ///      }
        ///     
        /// </remarks>
        /// <param name="groupId">Идентификатор группы</param>
        /// <param name="pageIndex">Номер страницы постраничного списка</param>
        /// <param name="pageSize">Размер страницы постраничного списка</param>
        /// <returns></returns>
        [HttpGet("{groupId}/Get")]
        public ActionResult<GroupUsersResponce> GetUsers(Guid groupId, int pageIndex = 1, int pageSize = 10)
        {
            var group = GetGroup(groupId, out var errorResponse);
            if (errorResponse != null)
            {
                return BadRequest(errorResponse);
            }

            var users = group.Users.Select(groupUser => _mapper.Map<User, GroupUsersResponce>(groupUser.User)).OrderBy(user => user.SecondName);

            return Ok(PaginatedList<GroupUsersResponce>.Create(users, pageIndex, pageSize).Items);
        }

        /// <summary>
        /// Удаление группы
        /// </summary>
        /// <remarks>
        /// 
        ///     Delete: api/Group/f30ac938-c8ad-46ce-8aa9-45103ce4b0c4
        ///     
        /// </remarks>
        /// <param name="groupId">Идентификатор группы</param>
        [HttpDelete("{groupId}")]
        public async Task<ActionResult> DeleteGroup(Guid groupId)
        {
            var group = GetGroup(groupId, out var errorResponse);
            if (errorResponse != null)
            {
                return BadRequest(errorResponse);
            }
            group.IsClosed = true;
            await _groupRepository.UpdateAsync(group);
            await _publishEndpoint.Publish(_mapper.Map<Group, GroupDeleted>(group));
            return Ok();
        }
        private User GetUser(Guid userId, out ErrorResponse errorResponse)
        {
            errorResponse = null;
            var user = _userRepository.GetByIdAsync(userId).Result;
            if (user == null)
            {
                errorResponse = new ErrorResponse
                {
                    Errors = new List<ErrorModel>
                    {
                        new ErrorModel
                        {
                            FieldName = "Идентификатор пользователя",
                            Message = $"По заданному параметру userId = {userId} пользователя не найдено"
                        }
                    }
                };
            }
            return user;
        }
        private Group GetGroup(Guid groupId, out ErrorResponse errorResponse)
        {
            errorResponse = null;
            var group = _groupRepository.GetByIdAsync(groupId).Result;
            if (group == null)
            {
                errorResponse = new ErrorResponse
                {
                    Errors = new List<ErrorModel>
                    {
                        new ErrorModel
                        {
                            FieldName = "Идентификатор группы",
                            Message = $"По заданному параметру groupId = {groupId} не найдено ни одной группы"
                        }
                    }
                };
            }
            return group;
        }
    }
}
