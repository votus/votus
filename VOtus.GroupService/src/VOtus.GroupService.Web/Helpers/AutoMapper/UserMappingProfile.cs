﻿using AutoMapper;
using BrokerContracts.Group;
using VOtus.GroupService.Core.Domain;
using VOtus.GroupService.Models;

namespace VOtus.GroupService.Helpers.AutoMapper
{
    public class UserMappingProfile : Profile
    {
        public UserMappingProfile()
        {
            CreateMap<User, GroupUsersResponce>()
                .ReverseMap();

/*            CreateMap<Group, GroupDeleted>()
                .ReverseMap();

            /*CreateMap<User, UserResponse>()
                .ReverseMap();

            CreateMap<User, UpdateUserRequest>()
                .ReverseMap();*/
        }
    }
}
