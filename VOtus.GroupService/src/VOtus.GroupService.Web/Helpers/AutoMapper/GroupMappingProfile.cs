﻿using AutoMapper;
using BrokerContracts.Group;
using VOtus.GroupService.Core.Domain;
using VOtus.GroupService.Models;

namespace VOtus.GroupService.Helpers.AutoMapper
{
    public class GroupMappingProfile : Profile
    {
        public GroupMappingProfile()
        {
            CreateMap<Group, GroupCreated>()
                .ReverseMap();

            CreateMap<Group, GroupDeleted>()
                .ReverseMap();

            CreateMap<Group, GroupResponce>()
                .ReverseMap();
        }
    }
}
