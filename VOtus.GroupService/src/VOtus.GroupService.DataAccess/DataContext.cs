﻿using Microsoft.EntityFrameworkCore;
using VOtus.GroupService.Core.Domain;

namespace VOtus.GroupService.DataAccess
{
    public class DataContext
        :DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Group> Groups { get; set; }

        public DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<GroupUser>().HasKey(i => new { i.GroupId, i.UserId});
        }
    }
}
